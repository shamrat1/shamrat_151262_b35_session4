<?php
namespace bitm; //start of namespace
echo'</br><h3 align="center"><b>PHP session 4</h3></b>'; //header

define('BITM','I am a student of BITM'); //use of define constant

echo BITM;
echo "</br>Namespace is ". __NAMESPACE__ ; //calling namespace
define('salary','5000$');

echo "</br>My salary is ".salary.".";
echo"</br>";
echo __DIR__; //directory contant, to know about current directory
echo"</br>";
echo __LINE__; // line constant shows the current line number
echo"</br>";

echo"</br>";

function atbitm(){
    echo "i am at_bitm"."</br>";
    echo __FUNCTION__; //__function__ constant shows the name of current function
}
echo " is the function name". atbitm() .".";
echo "</br>";
