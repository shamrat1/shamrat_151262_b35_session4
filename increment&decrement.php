<?php
echo "<h3 align='center'><b>Increment</b></h3>";
$var1= 2;

++$var1;

echo "after pre-increment of variable 1: ".$var1."</br>";

$var2 = 1;

$var2++;

echo "after post-increment of variable 2: ".$var2."</br>";

echo "<h3 align='center'><b>Decrement</b></h3>";
--$var1;

echo "after pre-decrement of variable 1: ".$var1."</br>";
$var2--;

echo "after post-decrement of variable 2: ".$var2."</br>";

